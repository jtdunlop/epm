namespace Dbsoft.Epm.Test
{
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Interfaces;
    using NSubstitute;

    public class ConfigurationServiceBuilder
    {
        private readonly IConfigurationService _config;

        public ConfigurationServiceBuilder()
        {
            _config = Substitute.For<IConfigurationService>();
        }

        public ConfigurationServiceBuilder WithSetting<T>(ConfigurationType type, T value) where T : struct
        {
            _config.GetSetting<T>(Arg.Any<string>(), type).Returns(value);
            return this;
        }

        public IConfigurationService Build()
        {
            return _config;
        }
    }
}