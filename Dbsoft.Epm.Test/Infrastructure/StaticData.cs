namespace Dbsoft.Epm.Test.Infrastructure
{
    public static class StaticData
    {
        public static class Items
        {
            private const int Tritanium = 34;
            private const int Zydrine = 39;
            private const int NanoFactory = 2869;
            private const int AmarrControlTower = 12235;

            public static class HighCollateral
            {
                public const int Id = Zydrine;
                public const decimal Price = 1500;
                public const decimal Volume = .01M;
            };

            public static class LowCollateral
            {
                public const int Id = Tritanium;
                public const decimal Price = 5;
                public const decimal Volume = .01M;
            };

            public static class BuyOrderMultiMaterial
            {
                public const int Id = NanoFactory;
                public const int ItemId = AmarrControlTower;
            }
        }
        public static class Regions
        {
            private const int Domain = 10000043;
            public static int Hub => Domain;
        }

        public static class SolarSystems
        {
            private const int Amarr = 30002187;
            public const int Buy = Amarr;
        }

        public static class Stations
        {
            private const int OrisEmperor = 60008494;
            private const int Orkashu = 60008170;
            public const int Buy = OrisEmperor;
            public const int Sell = OrisEmperor;
            public const int Build = Orkashu;
        }

        public static class Freight
        {
            public const int Jumps = 3;
        }
    }
}