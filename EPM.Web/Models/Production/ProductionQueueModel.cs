﻿namespace DBSoft.EPMWeb.Models.Production
{
	using AutoMapper;
	using EPM.DAL.DTOs;
    using EPM.DAL.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

	public class ProductionQueueModel
    {
		public ProductionQueueModel(IProductionQueueService service, string token)
		{
			Mapper.CreateMap<ProductionQueueDto, ProductionQueueItemModel>();

			Detail = Mapper.Map<IEnumerable<ProductionQueueDto>, IEnumerable<ProductionQueueItemModel>>(service
				.List(token)
				.Where(f => f.Quantity > 0)
				.OrderByDescending(f => f.ProfitFactor)
                .ThenByDescending(f => f.HourlyProfit));
		}

		public static string HelpUrl => "https://dbsoft.atlassian.net/wiki/display/EPM/Build+Items";
	    public IEnumerable<ProductionQueueItemModel> Detail { get; private set; }
	}
}
