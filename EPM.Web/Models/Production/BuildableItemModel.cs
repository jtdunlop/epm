﻿namespace DBSoft.EPMWeb.Models.Production
{
	using System.ComponentModel;

	public class BuildableItemModel
	{
		public BuildableItemModel()
		{
			ReturnUrl = "Index";
		}
		public int ItemID { get; set; }
		[DisplayName("Minimum Stock")]
		public int? MinimumStock { get; set; }
        public decimal? PerJobAdditionalCost { get; set; }
		public string ReturnUrl { get; set; }
	}
}