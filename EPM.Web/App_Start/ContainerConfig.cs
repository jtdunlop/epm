namespace DBSoft.EPMWeb
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Mvc;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Autofac.Integration.WebApi;
    using EPM.DAL;
    using EPM.DAL.Factories;
    using EPM.DAL.Services;
    using EPM.Logic;
    using EVEAPI.Crest.Facility;
    using EVEAPI.Crest.MarketOrder;
    using EVEAPI.Crest.SolarSystem;
    using Infrastructure;
    using Microsoft.AspNet.SignalR;
    using Models.Reporting;
    using AccountBalanceService = EVEAPI.Entities.AccountBalance.AccountBalanceService;

    public static class ContainerConfig
    {
        public static IContainer RegisterIocContainer(HttpConfiguration config)
        {
            var epm = new EpmConfig();
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterAssemblyTypes(typeof(ReportModelBuilder).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(EpmEntitiesFactory).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(EveApiImporter).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(MarketService).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(AccountBalanceService).Assembly)
                .Except<SolarSystemService>()
                .Except<FacilityService>()
                .AsImplementedInterfaces();

            builder.RegisterType<ImportManager>().As<IImportManager>().SingleInstance();

            if ( epm.GetSetting<bool>("UseMockCrestServices"))
            {
                builder.RegisterType<MockSolarSystemService>().As<ISolarSystemService>();
                builder.RegisterType<MockFacilityService>().As<IFacilityService>();
            }
            else
            {
                builder.RegisterType<SolarSystemService>().As<ISolarSystemService>();
                builder.RegisterType<FacilityService>().As<IFacilityService>();
            }

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            var users = container.Resolve<IUserService>();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => new HubUserIdProvider(users));
            
            var resolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = resolver;

            return container;
        }
    }
}