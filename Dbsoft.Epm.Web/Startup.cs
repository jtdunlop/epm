﻿namespace Dbsoft.Epm.Web
{
    using System;
    using System.Security.Claims;
    using Autofac;
    using Autofac.Framework.DependencyInjection;
    using DBSoft.EPM.Logic.Config;
    using DBSoft.EVEAPI.Crest;
    using Infrastructure;
    using Infrastructure.SignalR;
    using JetBrains.Annotations;
    using Microsoft.AspNet.Authentication.OAuth;
    using Microsoft.AspNet.Builder;
    using Microsoft.AspNet.Diagnostics;
    using Microsoft.AspNet.Hosting;
    using Microsoft.AspNet.Http;
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.SignalR;
    using Microsoft.Framework.Configuration;
    using Microsoft.Framework.DependencyInjection;
    using Microsoft.Framework.Logging;
    using Microsoft.Framework.Runtime;
    using Newtonsoft.Json.Serialization;
    using System.Linq;
    using DBSoft.EPM.DAL.Interfaces;
    using Infrastructure.Filters;

    [UsedImplicitly]
    public class Startup
    {
        private readonly ContainerConfig _containerConfig;

        public Startup(IHostingEnvironment env, IApplicationEnvironment appEnv)
        {
            // Setup configuration sources.
            var builder = new ConfigurationBuilder(appEnv.ApplicationBasePath)
                .AddJsonFile("config.json");
            builder.AddJsonFile(!env.IsDevelopment() ? "config.deploy.json" : "config.dev.json");
            builder.AddEnvironmentVariables();
            
            Configuration = builder.Build();
            _containerConfig = new ContainerConfig();
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime.
        [UsedImplicitly]
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add MVC services to the services container.
            services.AddMvc().Configure<MvcOptions>(options =>
            {
                options.OutputFormatters.OfType<JsonOutputFormatter>()
                       .First()
                       .SerializerSettings
                       .ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.Filters.Add(new ExceptionFilter());
            });
            
            services.Add(ServiceDescriptor.Singleton<IUserIdProvider, HubUserIdProvider>());
            services.AddSignalR();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddUserStore<ApplicationUserStore>()
                .AddRoleStore<ApplicationRoleStore>()
                .AddDefaultTokenProviders();

            services.Configure<Authentication>(Configuration.GetConfigurationSection("Authentication"));
            Container = ContainerConfig.BuildContainer(services, Configuration);
            services.AddSession();
            return Container.Resolve<IServiceProvider>();

            // Uncomment the following line to add Web API services which makes it easier to port Web API 2 controllers.
            // You will also need to add the Microsoft.AspNet.Mvc.WebApiCompatShim package to the 'dependencies' section of project.json.
            // services.AddWebApiConventions();
        }

        public static IContainer Container { get; set; }

        // Configure is called after ConfigureServices is called.
        [UsedImplicitly]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.MinimumLevel = LogLevel.Information;
            loggerFactory.AddConsole();

            // Configure the HTTP request pipeline.

            // Add the following to the request pipeline only in development environment.
            app.UseErrorPage(ErrorPageOptions.ShowAll);
            //if (env.IsDevelopment())
            //{
            //    app.UseBrowserLink();
            //    app.UseErrorPage(ErrorPageOptions.ShowAll);
            //}
            //else
            //{
            //    // Add Error handling middleware which catches all application specific errors and
            //    // send the request to the following path or controller action.
            //    app.UseErrorHandler("/Home/Index");
            //}

            // Add static files to the request pipeline.
            app.UseStaticFiles();
            
            app.UseIdentity();
            app.UseInMemorySession();
            app.UseSignalR();
            app.UseOAuthAuthentication("EVE Online", options =>
            {
                options.ClientId = Configuration["Authentication:EveSso:ClientId"];
                options.ClientSecret = Configuration["Authentication:EveSso:ClientSecret"];
                // This must not match the actual callback path. The middleware forwards the call to this endpoint to the action.
                options.CallbackPath = new PathString("/Account/ExternalLoginCallback");
                options.AuthorizationEndpoint = "https://login.eveonline.com/oauth/authorize";
                options.TokenEndpoint = "https://login.eveonline.com/oauth/token";
                options.Scope.Add("publicData");
                options.UserInformationEndpoint = "https://login.eveonline.com/oauth/verify";
                options.Notifications = new OAuthAuthenticationNotifications
                {
                    OnGetUserInformationAsync = async notification =>
                    {
                        var response = new AuthResponse
                        {
                            AccessToken = notification.AccessToken,
                            RefreshToken = notification.RefreshToken,
                            TokenType = notification.TokenType,
                            ExpiresIn = notification.ExpiresIn.GetValueOrDefault().Minutes
                        };

                        var user = await UserAuth.GetUser(response);

                        var identity = new ClaimsIdentity();
                        var claim = new Claim(ClaimTypes.NameIdentifier, user.CharacterName,
                            ClaimValueTypes.String, notification.Options.ClaimsIssuer);
                        identity.AddClaim(claim);
                        identity.AddClaim(new Claim(ClaimTypes.Name, user.CharacterName,
                                ClaimValueTypes.String, notification.Options.ClaimsIssuer));
                        claim = new Claim(ClaimTypes.UserData, notification.RefreshToken,
                            ClaimValueTypes.String, notification.Options.ClaimsIssuer);
                        identity.AddClaim(claim);

                        notification.Principal = new ClaimsPrincipal();
                        notification.Principal.AddIdentity(identity);
                    }
                };
            });


            // Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                // Uncomment the following line to add a route for porting Web API 2 controllers.
                // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
            });

            var config = Container.Resolve<IEpmConfig>();
            CacheConfig.Configure(config);
            MapperConfig.ConfigureMappings();
        }
    }
}
