namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System.Collections.Generic;
    using DBSoft.EPMWeb.Models.Reporting;

    public class TransactionAuditModel
    {
        public IEnumerable<TransactionAuditDetailModel> Detail { get; set; }
    }
}