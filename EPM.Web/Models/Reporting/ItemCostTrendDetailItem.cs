﻿namespace DBSoft.EPMWeb.Models.Reporting
{
	public class ItemCostTrendDetailItem
	{
		public string ItemName { get; set; }
		public string MaterialName { get; set; }
		public decimal Variance { get; set; }
        public decimal WeightedVariance { get; set; }
	}
}