﻿namespace DBSoft.EPMWeb.Controllers
{
	using System;
	using System.Web;
	using System.Web.Security;

	public class AuthenticationHelper
	{
		public static void SaveAuthenticationTicket(string token, HttpContextBase httpContext)
		{
			var ticket = new FormsAuthenticationTicket(1, "token", DateTime.Now,
			                                           DateTime.Now.AddMinutes(FormsAuthentication.Timeout.TotalMinutes),
			                                           true, token);
			var hashedTicket = FormsAuthentication.Encrypt(ticket);
			var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashedTicket);
			httpContext.Response.Cookies.Add(cookie);
		}

		public static string GetAuthenticationTicket(HttpContextBase httpContext)
		{
			try
			{
				var tokenCookie = httpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
				if (tokenCookie == null) return null;
				var ticket = FormsAuthentication.Decrypt(tokenCookie.Value);
				return ticket == null ? null : ticket.UserData;
			}
			catch
			{
				return null;
			}
		}
	}
}