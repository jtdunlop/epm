﻿namespace EPM.Test.Unit.Infrastructure
{
    using AutoMapper;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPMWeb;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class AutomapperTests
    {
        [TestMethod]
        public void TestMappings()
        {
            MapperConfig.ConfigureMappings();

            Mapper.AssertConfigurationIsValid();

            Mapper.Map<MarketAdjustedPriceDTO>(new MarketPrice());
        }
    }
}
