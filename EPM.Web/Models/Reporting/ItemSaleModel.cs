﻿namespace DBSoft.EPMWeb.Models.Reporting
{
    using EPM.DAL;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.Diagnostics;
	using System.Linq;
	using System.Linq.Expressions;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Routing;
	using EPM.DAL.Services.Transactions;
	using EPM.UI.PresentationRules;
    using ExpressionHelper = EPM.UI.ExpressionHelper;
    using EPM.UI;

	public class ItemSaleModel
	{
		public ItemSaleModel(IItemTransactionService service, DateTime @from, DateTime to, string token, string url)
		{
			Detail = service.ListByItem(new ItemTransactionRequest 
			{ 
				Token = token, 
				DateRange = new DateRange(from, to),
				TransactionType = TransactionType.Sell
			}).OrderByDescending(o => o.GPAmt);

			FromDate = from;
			ToDate = to;
            BaseUri = url;
			_tableDef.Columns.Add(new LinkColumnDefinition<ItemTransactionByItemDTO>(f => f.ItemName, 
				GetTransactionLink));
			_tableDef.Columns.Add(new DataColumnDefinition<ItemTransactionByItemDTO>(f => f.Quantity));
			_tableDef.Columns.Add(new DataColumnDefinition<ItemTransactionByItemDTO>(f => f.GrossAmount, null, new FooterDefinition<ItemSaleTotal>(f => f.GrossAmount)));
			_tableDef.Columns.Add(new DataColumnDefinition<ItemTransactionByItemDTO>(f => f.GPAmt, null, new FooterDefinition<ItemSaleTotal>(f => f.GrossProfit)));
			_tableDef.Columns.Add(new DataColumnDefinition<ItemTransactionByItemDTO>(f => f.GPPct, null, new FooterDefinition<ItemSaleTotal>(f => f.GPPct)));
		}
		public LinkDefinition GetTransactionLink(object rec)
		{
			var dsd = rec as ItemTransactionByItemDTO;

			// Refactor protection
			Expression<Func<ItemTransactionByItemDTO, object>> expr = f => f.ItemName;
			var name = ExpressionHelper.GetMemberName(expr);

			var data = ColumnRules<ItemTransactionByItemDTO>.GetFormattedString(dsd, expr);
			Debug.Assert(dsd != null, "dsd != null");
			var result = HtmlHelper.GenerateLink(HttpContext.Current.Request.RequestContext,
				RouteTable.Routes, data, "Default", "ItemTransaction", "Reporting",
				new RouteValueDictionary(
				new
				{
					dsd.ItemID,
					FromDate,
					ToDate
				}), null);
			return new LinkDefinition
			{
				Link = result,
				Alignment = ColumnRules<ItemTransactionByItemDTO>.Alignment(name)
			};
		}
		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, Detail, Total);
			}
		}
		private readonly TableDefinition _tableDef = new TableDefinition();
		public IEnumerable<ItemTransactionByItemDTO> Detail { get; set; }
		public ItemSaleTotal Total
		{
			get
			{
				return new ItemSaleTotal
				{
					GrossAmount = Detail.Sum(f => f.GrossAmount),
					GrossProfit = Detail.Sum(f => f.GPAmt)
				};
			}
		}
		[DataType(DataType.Date)]
		public DateTime	FromDate { get; set; }
		[DataType(DataType.Date)]
		public DateTime ToDate { get; set; }

        public string BaseUri { get; set; }
	}
}