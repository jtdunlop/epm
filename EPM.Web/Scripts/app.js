﻿require.config({
	paths: {
		'angular': 'lib/angular/angular',
		'angular-ui-date': 'lib/angular-ui-date/date',
		'jquery': 'lib/jquery/jquery',
		'jquery-ui': 'lib/jquery-ui/jquery-ui',
		'moment': 'lib/moment/moment'
	}
});

require(['angular-ui-date'], function () {
});