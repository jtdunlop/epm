﻿using DBSoft.EPM.DAL.Interfaces;

namespace DBSoft.EPMWeb.Models.Reporting
{
    using System;
    using AutoMapper;
	using EPM.DAL.DTOs;
	using System.Collections.Generic;
	using System.Linq;

	public class MaterialItemListItem
	{
		public int ItemID { get; set; }
		public string ItemName { get; set; }
		public decimal? BounceFactor { get; set; }
        public DateTime? LastModified { get; set; }
	}

	public class MaterialItemListModel
	{
		public MaterialItemListModel(IMaterialItemService service, string token, string returnUrl)
		{
			Mapper.CreateMap<MaterialItemDTO, MaterialItemListItem>();
			Detail = Mapper.Map<IEnumerable<MaterialItemDTO>, IEnumerable<MaterialItemListItem>>(service.List(token)).OrderBy(f => f.ItemName);
			ReturnUrl = returnUrl;
		}

		public IEnumerable<MaterialItemListItem> Detail { get; set; }
		public string ReturnUrl{get;set;}
	}
}