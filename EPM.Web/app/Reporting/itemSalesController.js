﻿angular.module('app').controller('itemSalesController', ['$scope', function ($scope) {
	$scope.model = window.model;

	$scope.refresh = function () {
		var params = $.param({ FromDate: moment($scope.model.FromDate).format(), ToDate: moment($scope.model.ToDate).format() });
		var uri = $scope.model.BaseUri + "?" + params;
		window.location = uri;
	};
}]);






