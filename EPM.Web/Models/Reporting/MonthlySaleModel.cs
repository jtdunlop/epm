﻿namespace DBSoft.EPMWeb.Models.Reporting
{
    using EPM.UI;
    using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using System.Web.Routing;
	using AutoMapper;
	using EPM.DAL;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
    using EPM.DAL.Services.Transactions;
	using Support.HtmlHelpers;

	public class MonthlySaleModel
	{
		private TableDefinition _tableDef;

		public MonthlySaleModel(IItemTransactionService service, string token)
		{
			Mapper.CreateMap<ItemTransactionByMonthDTO, DailySaleItemModel>();

			Detail = Mapper.Map<IEnumerable<ItemTransactionByMonthDTO>, IEnumerable<DailySaleItemModel>>(service
				.ListByMonth(new ItemTransactionRequest
				{
					Token = token,
					DateRange = new DateRange(DateTime.Now.AddMonths(-12).StartOfTheMonth(), DateTime.Now.AddMonths(-1).EndOfTheMonth()),
					TransactionType = TransactionType.Sell
				})
				.OrderByDescending(o => o.DateTime));
		}

		public IEnumerable<DailySaleItemModel> Detail { get; private set; }
		// ReSharper disable UnusedMember.Global
		public DateTime FromDate
		{
			get
			{
				return Detail.Min(f => f.DateTime);
			}
		}
		public DateTime ToDate
		{
			get
			{
				return Detail.Max(f => f.DateTime);
			}
		}
		public DailySaleTotal Total
		{
			get
			{
				return new DailySaleTotal
				{
					Sales = Detail.Sum(f => f.GrossAmount),
					Profit = Detail.Sum(f => f.GPAmt)
				};
			}
		}
		public string TableHtml
		{
			get
			{
				if (_tableDef == null)
				{
					_tableDef = new TableDefinition();
					_tableDef.Columns.Add(new LinkColumnDefinition<DailySaleItemModel>(f => f.DateTime,
						ItemSalesLink));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GrossAmount, null, new FooterDefinition<DailySaleTotal>(f => f.Sales)));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GPAmt, null, new FooterDefinition<DailySaleTotal>(f => f.Profit)));
					_tableDef.Columns.Add(new DataColumnDefinition<DailySaleItemModel>(f => f.GPPct, null, new FooterDefinition<DailySaleTotal>(f => f.GPPct)));
				}
				return TableHtmlGenerator.Generate(_tableDef, Detail, Total);
			}
		}

		private static LinkDefinition ItemSalesLink(object rec)
		{
			var dsd = rec as DailySaleItemModel;
			Debug.Assert(dsd != null, "dsd != null");
			var linkDef = HtmlUtilities.CreateLinkDefinition<DailySaleItemModel>(rec, "DateTime", "ItemSalesRange", "Reporting",
				new RouteValueDictionary(
					new
					{
						FromDate = dsd.DateTime,
						ToDate = dsd.DateTime.EndOfTheMonth()
					}));
			return linkDef;
		}
	}
}