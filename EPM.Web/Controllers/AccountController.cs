﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Annotations;
    using EPM.DAL.Interfaces;
    using EPM.DAL.Requests;
	using EPM.DAL.Services;
	using System.Web.Mvc;
	using System.Web.Security;
    using EVEAPI.Crest;
    using Models.Account;
    using NLog;

    public class AccountController : EpmController
	{
		private readonly IUserService _userService;
        private readonly IEpmConfig _config;
        private readonly IUserAuth _auth;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger(); 

        [UsedImplicitly]
	    public AccountController(IUserService userService, IEpmConfig config, IUserAuth auth)
	    {
	        _userService = userService;
	        _config = config;
            _auth = auth;
	    }

	    [AllowAnonymous]
		public ActionResult Login(string errorMessage)
		{
            ViewBag.Error = errorMessage;
            ViewBag.HideMenu = true;
            return View(new AccountLoginModel());
		}

        [AllowAnonymous]
        public async Task<ActionResult> EveApiCallback(string code, string state)
        {
            var user = await _auth.GetAuthenticatedUser(code, "authorization_code", _config.GetSetting("EveSsoClientId"), _config.GetSetting("EveSsoClientSecret"));
            var result = _userService.Authenticate(new SsoAuthenticateRequest
            {
                EveOnlineCharacter = user.CharacterName,
                RefreshToken = user.RefreshToken,
                IpAddress = Request.UserHostAddress
            });
            Logger.Info("{0} refresh token: {1}", user.CharacterName, user.RefreshToken );
            if (result.AuthenticationToken == null) return RedirectToAction("Login", new { result.ErrorMessage });
            AuthenticationHelper.SaveAuthenticationTicket(result.AuthenticationToken, HttpContext);

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login()
        {
            Debug.Assert(Request.Url != null, "Request.Url != null");
            var action = Url.Action("EveApiCallback", "Account", new { }, Request.Url.Scheme);
            var url = string.Format("https://login.eveonline.com/oauth/authorize/?response_type=code&redirect_uri={0}&client_id={1}&scope=publicData",
                action, _config.GetSetting("EveSsoClientId"));
            return Redirect(url);
        }


		public virtual ActionResult Logout(string errorMessage)
		{
			FormsAuthentication.SignOut();
			return RedirectToAction("Login", new { errorMessage });
		}

	}

 
}
