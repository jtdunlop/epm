﻿namespace EPM.Test.Unit.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Market;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class ItemServiceTests
    {
        private readonly EpmEntitiesFactory _factory;
        private readonly IConfigurationService _config;
        private readonly IUserService _users;
        private readonly IBlueprintService _blueprints;

        public ItemServiceTests()
        {
            const int raven = 638;
            const int ravenBlueprint = 688;

            const int orkashu = 60008170;

            var config = new EpmConfig();
            var provider = new ConnectionStringProvider(config);
            _factory = new EpmEntitiesFactory(provider);

            _config = Substitute.For<IConfigurationService>();
            _config.GetSetting<int>(Arg.Any<string>(), ConfigurationType.FactoryLocation).Returns(orkashu);
            _users = Substitute.For<IUserService>();
            _users.GetUserID(Arg.Any<string>()).Returns(1);

            _blueprints = Substitute.For<IBlueprintService>();
            _blueprints.List().Returns(new List<BlueprintDTO> { new BlueprintDTO { BuildItemID = raven, ItemID = ravenBlueprint } });

        }

        // Test everything that returns a List<ItemDTO> or List<BuildableItemDTO>
        [TestMethod]
        public void List_ShouldIncludeVolumes()
        {
            var service = new ItemService(_factory, null, null, _users);
            var result = service.List(new ItemRequest());
            Assert.IsTrue(result.Any(f => f.Volume != 0));
        }

        [TestMethod]
        public void ListProducible_ShouldIncludeVolumes()
        {
            var service = new ItemService(_factory, null, _blueprints, _users);
            var result = service.ListProducible();
            Assert.IsTrue(result.Any(f => f.Volume != 0));
        }


        [TestMethod]
        public void ListProducibleMaterials_ShouldIncludeVolumes()
        {
            var service = new ItemService(_factory, null, _blueprints, null);
            var result = service.ListProducibleMaterials();
            Assert.IsTrue(result.First().Volume != 0);
        }

        [TestMethod]
        public void ListBuildable_ShouldIncludeVolumes()
        {
            var service = new ItemService(_factory, _config, null, _users);
            var result = service.ListBuildable(null);
            Assert.IsTrue(result.First().Volume != 0);
        }

        [TestMethod]
        public void ListMaintainable_ShouldIncludeVolumes()
        {
            var service = new ItemService(_factory, _config, null, _users);
            var result = service.ListMaintainable(null);
            Assert.IsTrue(result.First().Volume != 0);
        }
    
    }
}
