﻿using DBSoft.EPM.DAL.Interfaces;
using DBSoft.EPM.UI;

namespace DBSoft.EPMWeb.Models.Production
{
	using System.Diagnostics;
	using AutoMapper;
	using EPM.DAL.DTOs;
	using EPM.DAL.Services;
	using System.Collections.Generic;
	using System.Linq;
	using Support.HtmlHelpers;
	using UI;

	public class ProductionMaterialItemModel
	{
		public int ItemID { get; set; }
		public string ItemName { get; set; }
		public long Needed { get; set; }
		public long Available { get; set; }
		public string ReturnUrl { get; set; }
	}

	public class ProductionMaterialModel 
	{
		private readonly TableDefinition _tableDef;
		private readonly IEnumerable<ProductionMaterialItemModel> _detail;

		public ProductionMaterialModel(IProductionMaterialService service, string token, string returnUrl)
		{
			Mapper.CreateMap<ProductionMaterialDto, ProductionMaterialItemModel>()
				.ForMember(m => m.Available, opt => opt.MapFrom(m => m.Inventory))
				.ForMember(m => m.Needed, opt => opt.MapFrom(m => m.Required))
				.ForMember(m => m.ReturnUrl, opt => opt.ResolveUsing(f => returnUrl));

			_detail = Mapper.Map<IEnumerable<ProductionMaterialDto>, IEnumerable<ProductionMaterialItemModel>>(service.List(token)
				.OrderBy(f => f.ItemName));

			_tableDef = new TableDefinition
				{
					Columns = new List<IColumnDefinition>
					{
						new LinkColumnDefinition<ProductionMaterialItemModel>(f => f.ItemName, CreateLink),
						new DataColumnDefinition<ProductionMaterialItemModel>(f => f.Needed),
						new DataColumnDefinition<ProductionMaterialItemModel>(f => f.Available)
					}
				};

			ReturnUrl = returnUrl;
		}

		private static LinkDefinition CreateLink(object rec)
		{
			var pmi = rec as ProductionMaterialItemModel;
			Debug.Assert(pmi != null, "dsd != null");

			var htmlAttributes = pmi.Available < pmi.Needed ?
				new Dictionary<string, object> { { "class", "error-link" } } :
				null;
			var routes = new { pmi.ItemID, pmi.ReturnUrl };

			var linkDef = HtmlUtilities.CreateLinkDefinition<ProductionMaterialItemModel>(
				rec,
				() => pmi.ItemName,
				"MaterialItem",
				"Production",
				routes, 
				htmlAttributes);
			return linkDef;
		}

		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}

		public string HelpUrl { get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Material"; } }

		private string ReturnUrl { get; set; }
	}
}