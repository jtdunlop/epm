﻿namespace DBSoft.EPMWeb
{
    using AutoMapper;
    using EPM.DAL.CodeFirst.Models;
    using EPM.DAL.DTOs;
    using Models.Production;

    public static class MapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<EveApiStatusDTO, ApiStatusItemModel>();
            Mapper.CreateMap<Station, StationDTO>()
                .ForMember(m => m.StationID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.StationName, opt => opt.MapFrom(m => m.Name))
                .ForMember(m => m.StationTax, opt => opt.MapFrom(m => m.Tax))
                ;
            Mapper.CreateMap<MarketPrice, MarketAdjustedPriceDTO>();
            Mapper.CreateMap<SolarSystem, SolarSystemDTO>()
                .ForMember(m => m.SolarSystemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.SolarSystemName, opt => opt.MapFrom(m => m.Name))
                ;
        }
    }
}
