﻿namespace DBSoft.EPMWeb
{
    using System.Web.Optimization;
    using Autofac;
	using DbSoft.Cache.Aspect;
    using EPM.DAL.Commands;
	using EPM.DAL.Interfaces;
	using NLog;
	using NLog.Targets;
	using Support.Filters;
	using Support.ModelBinders;
	using System;
	using System.Web.Mvc;
	using System.Web.Routing;
    using System.Web.Http;
    using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

    public class MvcApplication : System.Web.HttpApplication
	{
		private static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new ErrorHandlerAttribute());
			// filters.Add(new RequireHttpsAttribute());
			filters.Add(new AuthorizeAttribute());
		}

		private static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Default", // Route name
				"{controller}/{action}/{id}", // URL with parameters
				new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			);

		}

		protected void Application_Start()
		{
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MapperConfig.ConfigureMappings();

 			CacheProvider.Cache = new CacheProvider();
			CacheService.Cache = CacheProvider.Cache;
			CacheService.SessionProperty = "token";

            GlobalConfiguration.Configure(WebApiConfig.Register);
            var container = ContainerConfig.RegisterIocContainer(GlobalConfiguration.Configuration);

            AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);

			ModelBinders.Binders.Add(typeof(DateTime), new DateTimeBinder());
			ModelBinders.Binders.Add(typeof(DateTime?), new NullableDateTimeBinder());

			ConfigureLogTargets(container);
		}

	    private static void ConfigureLogTargets(IComponentContext container)
		{
            GlobalDiagnosticsContext.Set("Application", "Webjob");
			var config = LogManager.Configuration;
			var target = (DatabaseTarget)config.FindTargetByName("database");
			target.ConnectionString = new ConnectionStringBuilder(container.Resolve<IEpmConfig>()).Build();
			LogManager.ReconfigExistingLoggers();
		}
	}
}