﻿namespace DBSoft.EPMWeb.Models.Reporting
{
	using System.Collections.Generic;
	using System.Linq;
	using EPM.DAL.Services;
	using EPM.UI;

	public class ItemCostTrendDetailModel
	{
		private readonly IMaterialVarianceService _service;
		private readonly string _token;
		private readonly TableDefinition _tableDef;

		public ItemCostTrendDetailModel(IMaterialVarianceService service, string itemName, string token)
		{
			_service = service;
			_token = token;
			ItemName = itemName;

			_tableDef = new TableDefinition
				{
					Columns = new List<IColumnDefinition> 
						{ 
							new DataColumnDefinition<ItemCostTrendDetailItem>(f => f.MaterialName),
							new DataColumnDefinition<ItemCostTrendDetailItem>(f => f.Variance),
							new DataColumnDefinition<ItemCostTrendDetailItem>(f => f.WeightedVariance)
						}
				};
		}

		public string ItemName { get; set; }
		public string TableHtml
		{
			get
			{
				var detail = _service
					.ListVariancesByItem(_token)
					.Where(f => f.ItemName == ItemName)
					.OrderByDescending(f => f.WeightedVariance);

				return TableHtmlGenerator.Generate(_tableDef, detail);
			}
		} 
	}
}