﻿namespace DBSoft.EPMWeb.Models.Production
{
    using AutoMapper;
    using EPM.DAL.DTOs;
    using EPM.DAL.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using EPM.UI;


    public class PostSellOrdersModel
	{
		public PostSellOrdersModel(IMarketRestockService restockService, string token)
		{
			Mapper.CreateMap<MarketRestockDTO, PostSellOrdersItemModel>();

			Detail = Mapper.Map<IEnumerable<MarketRestockDTO>, IEnumerable<PostSellOrdersItemModel>>(restockService.List(token)
                .OrderBy(o => o.ItemName));
			Table.Columns.Add(new DataColumnDefinition<PostSellOrdersItemModel>(f => f.ItemName));
			Table.Columns.Add(new DataColumnDefinition<PostSellOrdersItemModel>(f => f.NewPrice));
			Table.Columns.Add(new DataColumnDefinition<PostSellOrdersItemModel>(f => f.MarketPrice));
			Table.Columns.Add(new DataColumnDefinition<PostSellOrdersItemModel>(f => f.Markup));
		}

        private static string GetClass(object rec)
        {
            var model = rec as PostSellOrdersItemModel;
            if ( model != null && model.Timestamp < DateTime.UtcNow.AddHours(-4) )
            {
                return "class=\"error\"";
            }
            return "";
        }

        public IEnumerable<PostSellOrdersItemModel> Detail { get; private set; }
        public readonly TableDefinition Table = new TableDefinition
        {
            GetClass = GetClass
        };
	}
}
