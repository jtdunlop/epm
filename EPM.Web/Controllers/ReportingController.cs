﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Annotations;
    using System;
	using System.Web.Mvc;
    using EPM.DAL.Services;
    using EPM.DAL.Services.ItemCosts;
    using EPM.DAL.Services.Transactions;
    using Models.Reporting;
    using Support.Filters;

	[RequireDatabaseAuthentication]
	public partial class ReportingController : EpmController
	{
		private readonly IItemTransactionService _sales;
		private readonly IItemCostService _itemCostService;
	    private readonly IMaterialVarianceService _variances;
	    private readonly IReportModelBuilder _reportModelBuilder;

        [UsedImplicitly]
        public ReportingController(IItemTransactionService sales, IItemCostService itemCostService, IMaterialVarianceService variances, 
            IReportModelBuilder reportModelBuilder)
        {
            _sales = sales;
            _itemCostService = itemCostService;
            _variances = variances;
            _reportModelBuilder = reportModelBuilder;
        }

		public virtual ActionResult Index()
		{
			return View();
		}

		public virtual ActionResult ItemCostTrend()
		{
			return View(new ItemCostTrendModel(_itemCostService, Token));
		}

		public virtual ActionResult MaterialCostTrend()
		{
			return View(new MaterialCostTrendModel(_variances, Token));
		}

		public virtual ActionResult ItemCostTrendDetail(string itemName)
		{
			return View(new ItemCostTrendDetailModel(_variances, itemName, Token));
		}

		public virtual ActionResult ItemSalesRange(DateTime fromDate, DateTime toDate)
		{
		    Debug.Assert(Request.Url != null, "Request.Url != null");
		    var model = new ItemSaleModel(_sales, fromDate, toDate, Token, string.Format("{0}://{1}/{2}", 
                Request.Url.Scheme, Request.Url.Host, Request.Url.AbsolutePath));
			return View("ItemSales", model);
		}

		public virtual ActionResult ItemSales(DateTime? fromDate, DateTime? toDate)
		{
			fromDate = fromDate ?? DateTime.UtcNow.AddDays(-7);
			toDate = toDate ?? DateTime.UtcNow.AddDays(-1);
			return ItemSalesRange(fromDate.Value, toDate.Value);
		}

		public virtual ActionResult ItemTransaction(string itemID, DateTime fromDate, DateTime toDate)
		{
			var model = new ItemTransactionModel(_sales, int.Parse(itemID), fromDate, toDate, Token);
			return View(model);
		}

		public virtual ActionResult DailySales()
		{
			var model = new DailySaleModel(_sales, Token);
			return View(model);
		}

		public virtual ActionResult MonthlySales()
		{
			var model = new MonthlySaleModel(_sales, Token);
			return View(model);
		}

        public async Task<ActionResult> MarketResearch()
        {
            var model = await Task.Run(() => _reportModelBuilder.CreateMarketResearchModel(Token));
            return View(model);
        }

        public ActionResult TransactionAudit()
        {
            var model = _reportModelBuilder.CreateTransactionAuditModel(Token);
            return View(model);
        }

        public ActionResult SubscriberSales(DateTime? fromDate, DateTime? toDate)
        {
            var model = _reportModelBuilder.CreateSubscriberSalesModel(Token, fromDate, toDate);
            return View(model);
        }
	}
}
