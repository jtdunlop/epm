namespace DBSoft.EPMWeb.Models.Reporting
{
    using System.Collections.Generic;

    public class TransactionAuditModel
    {
        public IEnumerable<TransactionAuditDetailModel> Detail { get; set; }
    }
}