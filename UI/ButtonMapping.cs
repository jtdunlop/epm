﻿using System.Windows.Forms;

namespace DBSoft.UI
{
	public class ButtonMapping
	{
		public Button MappedButton { get; set; }
		public string EnabledProperty { get; set; }
		public ControllerAction Action { get; set; }
	}
}