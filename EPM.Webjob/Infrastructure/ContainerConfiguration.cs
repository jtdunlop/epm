namespace EPM.Webjob.Infrastructure
{
    using Autofac;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EVEAPI.Crest.Facility;
    using DBSoft.EVEAPI.Crest.MarketOrder;
    using DBSoft.EVEAPI.Crest.SolarSystem;

    public static class ContainerConfiguration
    {
        public static IContainer RegisterIocContainer()
        {
            var builder = new ContainerBuilder();
            // DBSoft.EPM.DAL
            builder.RegisterAssemblyTypes(typeof(EpmEntitiesFactory).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterType<EpmConfig>().As<IEpmConfig>();
            // DBSoft.EVEAPI
            builder.RegisterAssemblyTypes(typeof(MarketService).Assembly)
                .Except<SolarSystemService>()
                .Except<FacilityService>()
                .Except<MarketService>()
                .AsImplementedInterfaces();

            var config = new EpmConfig();
            if (config.GetSetting<bool>("UseMockCrestServices"))
            {
                builder.RegisterType<MockSolarSystemService>().As<ISolarSystemService>();
                builder.RegisterType<MockFacilityService>().As<IFacilityService>();
                // builder.RegisterType<MockMarketService>().As<IMarketService>();
            }
            else
            {
                builder.RegisterType<SolarSystemService>().As<ISolarSystemService>();
                builder.RegisterType<FacilityService>().As<IFacilityService>();
                builder.RegisterType<MarketService>().As<IMarketService>();
            }
            
                
            var container = builder.Build();
            return container;
        }
    }
}