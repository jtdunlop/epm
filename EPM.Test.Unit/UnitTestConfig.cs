namespace EPM.Test.Unit
{
    using System.Collections.Generic;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Interfaces;
    using NSubstitute;

    public class UnitTestConfig
    {
        public UnitTestConfig()
        {
            Universe = Substitute.For<IUniverseService>();
            Config = Substitute.For<IConfigurationService>();
        }

        public UnitTestConfig MultiRegion()
        {
            Universe = CreateUniverse();
            Config = CreateMultiRegionConfig();
            return this;
        }

        public UnitTestConfig SingleRegion()
        {
            Universe = CreateUniverse();
            Config = CreateSingleRegionConfig();
            return this;
        }

        private static IUniverseService CreateUniverse()
        {
            var universe = Substitute.For<IUniverseService>();
            universe.ListRegions().Returns(f => new List<RegionDTO>
            {
                new RegionDTO
                {
                    RegionName = "Domain",
                    RegionID = 1
                },
                new RegionDTO
                {
                    RegionName = "The Forge",
                    RegionID = 2
                }
            });
            universe.GetStationSolarSystem(1).Returns(f => new SolarSystemDTO
            {
                RegionID = 1
            });
            universe.GetStationSolarSystem(2).Returns(f => new SolarSystemDTO
            {
                RegionID = 2
            });
            return universe;
        }
        private static IConfigurationService CreateSingleRegionConfig()
        {
            var config = CreateConfig();
            config.GetSetting<int>(Arg.Any<string>(), ConfigurationType.FactoryLocation).Returns(1); // Domain
            config.GetSetting<int>(Arg.Any<string>(), ConfigurationType.MarketSellLocation).Returns(1); // The Forge
            return config;
        }
        private static IConfigurationService CreateMultiRegionConfig()
        {
            var config = CreateConfig();
            config.GetSetting<int>(Arg.Any<string>(), ConfigurationType.FactoryLocation).Returns(1); // Domain
            config.GetSetting<int>(Arg.Any<string>(), ConfigurationType.MarketSellLocation).Returns(2); // The Forge
            return config;
        }
        private static IConfigurationService CreateConfig()
        {
            var config = Substitute.For<IConfigurationService>();
            return config;
        }

        public IUniverseService Universe { get; private set; }
        public IConfigurationService Config { get; private set; }
    }
}