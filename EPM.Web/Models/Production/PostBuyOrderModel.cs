﻿
namespace DBSoft.EPMWeb.Models.Production
{
    using System;
    using System.Diagnostics;
    using AutoMapper;
    using EPM.DAL.DTOs;
    using EPM.DAL.Interfaces;
    using EPM.DAL.Requests;
    using System.Collections.Generic;
    using System.Linq;
    using EPM.UI;

    public class PostBuyOrderModel
    {
        private readonly TableDefinition _tableDef;
        private readonly IEnumerable<PostBuyOrderItemModel> _detail;

        public PostBuyOrderModel(IMaterialPurchaseService service, string token, bool igb)
        {
            Mapper.CreateMap<MaterialPurchaseDTO, PostBuyOrderItemModel>();

            _detail = Mapper.Map<IEnumerable<MaterialPurchaseDTO>, IEnumerable<PostBuyOrderItemModel>>(service.List(new MaterialPurchaseRequest
            {
                Token = token,
                IncludeMaterialsWithActiveOrders = false
            }))
                .Where(f => f.Percentage < 100)
                .OrderBy(f => f.Percentage);
            _tableDef = new TableDefinition
            {
                Columns = new List<IColumnDefinition>
				{
                    igb ? (IColumnDefinition) new LinkColumnDefinition<PostBuyOrderItemModel>(f => f.ItemName, CreateLink) : 
                        new DataColumnDefinition<PostBuyOrderItemModel>(f => f.ItemName), 
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.NewPrice),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.MarketPrice),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.RangePrice),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.PurchaseQuantity),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.UsageQuantity),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.InventoryQuantity),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.FactoryQuantity),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.FactoryRequired),
					new DataColumnDefinition<PostBuyOrderItemModel>(f => f.Percentage)
				},
                GetClass = GetClass
            };
        }

        private static LinkDefinition CreateLink(object rec)
        {
            var pmi = rec as PostBuyOrderItemModel;
            Debug.Assert(pmi != null, "dsd != null");

            var linkDef = new LinkDefinition
            {
                Link = string.Format("<a href=javascript:CCPEVE.buyType({1}) {2}>{0}</a>", pmi.ItemName, pmi.ItemId, GetLinkClass(pmi))
            };
            return linkDef;
        }

        private static string GetLinkClass(PostBuyOrderItemModel pmi)
        {
            return pmi.Timestamp < DateTime.UtcNow.AddHours(-4) ? "class='error-link'" : "";
        }


        private string GetClass(object rec)
        {
            var model = rec as PostBuyOrderItemModel;
            if (model != null && model.Timestamp < DateTime.UtcNow.AddHours(-4))
            {
                return "class='error'";
            }
            return "";
        }

        public string TableHtml
        {
            get
            {
                return TableHtmlGenerator.Generate(_tableDef, _detail);
            }
        }

        public static string HelpUrl
        {
            get { return "https://dbsoft.atlassian.net/wiki/display/EPM/Post+Buy+Orders"; }
        }
    }
}