﻿using System.Windows.Forms;

namespace DBSoft.UI
{
	public class FieldMapping
	{
		public TextBox EditField { get; set; }
		public string FieldName { get; set; }
		public string FormatString { get; set; }
		public bool ForceReadOnly { get; set; }
	}
}