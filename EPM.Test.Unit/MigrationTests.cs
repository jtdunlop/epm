﻿namespace EPM.Test.Unit
{
    using System.IO;
    using System.Linq;
    using System.Text;
    using DBSoft.EPM.DAL.CodeFirst.Migrations;
    using DBSoft.EVE.SDE;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class MigrationTests
    {
        [TestMethod]
        public void Migration_ShouldLoad()
        {
            var migration = new StaticDataMigration();
            migration.Up();
        }

        [TestClass]
        public class BlueprintServiceTests
        {
            [TestMethod]
            public void FullManifestYaml_ShouldLoad()
            {
                var provider = new YamlProvider(Settings.Blueprints);
                var service = new BlueprintService(provider);
                var result = service.GetManifests();
                Assert.IsTrue(result.Any());
            }

            [TestMethod]
            public void UnbuildableItems_ShouldBeSkipped()
            {
                // E.g. rookie ships
                const string yaml = @"
935:
    activities:
        copying:
            time: 4800
        research_material:
            time: 2100
        research_time:
            time: 2100
    blueprintTypeID: 935
    maxProductionLimit: 30
";
                var stream = CreateStream(yaml);
                var provider = Substitute.For<IYamlProvider>();
                provider.GetYamlStream().Returns(stream);
                var service = new BlueprintService(provider);
                var result = service.GetManifests();
                Assert.IsTrue(!result.Any());
            }

            [TestMethod]
            public void ItemsWithoutMaterials_ShouldBeSkipped()
            {
                // E.g. Minmatar Peacekeeper which isn't even buildable
                const string yaml = @"
947:
    activities:
        copying:
            time: 4800
        manufacturing:
            products:
            -   quantity: 1
                typeID: 600
            skills:
            -   level: 1
                typeID: 3380
            time: 6000
        research_material:
            time: 2100
        research_time:
            time: 2100
    blueprintTypeID: 947
    maxProductionLimit: 30
";
                var stream = CreateStream(yaml);
                var provider = Substitute.For<IYamlProvider>();
                provider.GetYamlStream().Returns(stream);
                var service = new BlueprintService(provider);
                var result = service.GetManifests();
                Assert.IsTrue(!result.Any());
            }
        }

        [TestClass]
        public class ItemServiceTests
        {
            [TestMethod]
            public void FullManifestYaml_ShouldLoad()
            {
                var provider = new YamlProvider(Settings.Items);
                var service = new ItemService(provider);
                var result = service.GetItems();
                Assert.IsTrue(result.Any());
            }

            [TestMethod]
            public void VolumelessItems_ShouldBeExcluded()
            {
                const string yaml = @"
0:
    groupID: 0
    mass: 1.0
    name:
        de: '#System'
        en: '#System'
    portionSize: 1
    published: false
";
                var stream = CreateStream(yaml);
                var provider = Substitute.For<IYamlProvider>();
                provider.GetYamlStream().Returns(stream);
                var service = new ItemService(provider);
                var result = service.GetItems();
                Assert.AreEqual(result.Count(), 0);
            }
        }

        private static MemoryStream CreateStream(string yaml)
        {
            var byteArray = Encoding.ASCII.GetBytes(yaml);
            var stream = new MemoryStream(byteArray);
            return stream;
        }
    }
}
