﻿namespace DBSoft.EPMWeb.Models.Account
{
	using System.Collections.Generic;

	public class AccountsModel
	{
		public List<AccountModel> Accounts { get; set; } 
	}
}