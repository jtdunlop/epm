namespace Dbsoft.Epm.Test
{
    using DBSoft.EPM.DAL.Interfaces;
    using NSubstitute;

    public class EpmConfigBuilder
    {
        private readonly IEpmConfig _config;

        public EpmConfigBuilder()
        {
            _config = Substitute.For<IEpmConfig>();
        }

        public EpmConfigBuilder WithSetting(string setting, string value)
        {
            _config.GetSetting(setting).Returns(value);
            return this;
        }

        public EpmConfigBuilder WithConnectionString(string name, string value)
        {
            _config.GetConnectionString(name).Returns(value);
            return this;
        }

        public IEpmConfig Build()
        {
            return _config;
        }
    }
}