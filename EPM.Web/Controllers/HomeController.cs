﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Collections.Generic;
    using EPM.DAL.Services.AccountApi;
    using Annotations;
    using EVEAPI.Entities.Account;
    using EPM.DAL.Services;
    using Models.Home;
	using Support.Filters;
	using System.Linq;
	using System.Web.Mvc;

	[RequireDatabaseAuthentication]
	public class HomeController : EpmController
	{
	    private readonly IAccountApiService _accounts;
	    private readonly IUserService _users;

	    [UsedImplicitly]
	    public HomeController(IAccountApiService accounts, IUserService users)
		{
	        _accounts = accounts;
	        _users = users;
		}

		[RequireCompleteConfiguration]
		public ActionResult Index(string impersonate)
		{
            if ( !string.IsNullOrEmpty(impersonate))
            {
                _users.Impersonate(impersonate, Token);
            }
			var accounts = _accounts.List(Token);

			var model = new DashboardModel
			{
                DashboardImages = GetDashboardImages(accounts),
				HelpUrl = "https://dbsoft.atlassian.net/wiki/display/EPM/Dashboard",
                IsAdmin = _users.IsAdmin(Token)
			};
			return View(model);
		}

	    private static IEnumerable<DashboardImage> GetDashboardImages(IEnumerable<AccountApiDTO> accounts)
	    {
            return accounts.Select(account => new DashboardImage
            {
                CharacterName = account.AccountName,
                ImageUrl = string.Format("http://image.eveonline.com/{1}/{0}_128.{2}",
                    account.EveApiID, 
                    account.ApiKeyType == ApiKeyType.Character ? "Character" : "Corporation",
                    account.ApiKeyType == ApiKeyType.Character ? "jpg" : "png")
            }).ToList();
	    }
	}
}
