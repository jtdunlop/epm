﻿using DBSoft.EPM.DAL.Services.AccountApi;

namespace DBSoft.EPMWeb.Support.Filters
{
	using System.Web.Mvc;
	using System.Web.Routing;
	using Controllers;
	using EPM.DAL;
	using EPM.DAL.Factories;
	using EPM.DAL.Services;
	using EPM.Logic;

	public class RequireCompleteConfigurationAttribute : ActionFilterAttribute 
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
            var factory = new EPMEntitiesFactory(new ConnectionStringProvider(new EPMConfig()));
			var configProcessor = new ConfigurationProcessor(new ConfigurationService(factory), new AccountApiService(factory), new UserService(factory));
		
			var token = AuthenticationHelper.GetAuthenticationTicket(filterContext.HttpContext);
			if (!UserService.UserIsAuthenticated(token))
			{
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
					{
						action = "Logout",
						controller = "Account"
					}));
			}
			else if (!configProcessor.AccountIsAvailable(token))
			{
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
				{
					action = "Accounts",
					controller = "Maintenance"
				}));
			}
			else if (!configProcessor.ConfigurationIsValid(token))
			{
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
					{
						action = "Configuration", 
						controller = "Maintenance"
					}));
			}
			base.OnActionExecuting(filterContext);
		}
	}
}