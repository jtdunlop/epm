namespace Dbsoft.Epm.Webjob.Infrastructure
{
    using Autofac;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Mvc6;
    using DBSoft.EVEAPI.Crest.Facility;
    using DBSoft.EVEAPI.Crest.MarketOrder;
    using DBSoft.EVEAPI.Crest.SolarSystem;
    using Microsoft.Framework.Configuration;

    public static class ContainerConfiguration
    {
        public static IContainer RegisterIocContainer()
        {
            var builder = new ContainerBuilder();
            // DBSoft.EPM.DAL
            builder.RegisterAssemblyTypes(typeof(EpmEntitiesFactory).Assembly)
                .AsImplementedInterfaces();
            builder.RegisterType<EpmConfig>().As<IEpmConfig>();
            // DBSoft.EVEAPI
            builder.RegisterAssemblyTypes(typeof(MarketService).Assembly)
                .Except<SolarSystemService>()
                .Except<FacilityService>()
                .Except<MarketService>()
                .AsImplementedInterfaces();

            builder.Register(c => BuildConfiguration()).As<IConfiguration>();

            // builder.RegisterType<ConfigurationSection>().As<IConfiguration>();

            var container = builder.Build();
            return container;
        }

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder(".")
                .AddJsonFile("config.json");
            var result = builder.Build();
            return result;
        }
    }
}