using System;
using System.ComponentModel;

namespace DBSoft.EPMWeb.Models.Production
{
    public class PostSellOrdersItemModel
    {
        public string ItemName { get; set; }
        public decimal NewPrice { get; set; }
        [DisplayName("Market Price")]
        public decimal MarketPrice { get; set; }
        public decimal Markup { get; set; }
        public DateTime Timestamp { get; set; }
    }
}