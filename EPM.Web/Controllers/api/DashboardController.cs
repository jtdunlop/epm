﻿using System.Web.Http;

namespace DBSoft.EPMWeb.Controllers.api
{
    using EPM.DAL.Interfaces;
    using EPM.DAL.Services.Transactions;

    public class DashboardController : ApiController
    {
        private readonly DashboardModelBuilder _dashboardModelBuilder;

        public DashboardController(IAccountBalanceService accountBalanceService, IItemTransactionService itemTransactionService,
            IAssetCapitalService assetCapitalService)
        {
            _dashboardModelBuilder = new DashboardModelBuilder(accountBalanceService, itemTransactionService, assetCapitalService);
        }

        public IHttpActionResult Get(string token)
        {
            var model = _dashboardModelBuilder.CreateModel(token);
            return Ok(model);
        }
    }
}
