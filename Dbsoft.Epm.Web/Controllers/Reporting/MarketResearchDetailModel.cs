namespace DBSoft.EPMWeb.Models.Reporting
{
    public class MarketResearchDetailModel
    {
        public string ItemName { get; set; }
        public decimal IskPerHour { get; set; }
        public decimal Margin { get; set; }
        public decimal Markup { get; set; }
        public decimal Volume { get; set; }
        public decimal Competitors { get; set; }
        public decimal ProfitFactor { get; set; }
        public bool IsMine { get; set; }
    }
}