﻿using NLog;

namespace DBSoft.EPMWeb.Support.Filters
{
	using System.Web.Mvc;
	using System.Web.Routing;
	using Controllers;
	using EPM.DAL.Services;

	public class RequireDatabaseAuthenticationAttribute : ActionFilterAttribute
	{
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger(); 

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var token = AuthenticationHelper.GetAuthenticationTicket(filterContext.HttpContext);
			if (!UserService.UserIsAuthenticated(token))
			{
                Logger.Info("Token {0} not found", token);
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
				{
					action = "Logout",
					controller = "Account",
					errorMessage = "Session reset due to inactivity or application restart"
				}));
			}
			base.OnActionExecuting(filterContext);
		}
	}
}