﻿using System.Windows.Forms;

namespace DBSoft.UI
{
	internal static class ControlExtensions
	{
		public static bool CanEdit(this Control control)
		{
			// Exclude containers
			if ( control.Controls.Count > 0 || !control.TabStop || !control.CanFocus )
			{
				return false;
			}
			if ( control is TextBox )
			{
				var textbox = control as TextBox;
				return !textbox.ReadOnly;
			}
			else
			{
				return true;
			}
		}
	}
}