﻿namespace DBSoft.EPMWeb.Infrastructure
{
    using System.Web.Security;
    using EPM.DAL.Services;
    using Microsoft.AspNet.SignalR;

    public class HubUserIdProvider : IUserIdProvider
    {
        private readonly IUserService _users;

        public HubUserIdProvider(IUserService users)
        {
            _users = users;
        }

        public string GetUserId(IRequest request)
        {
            var token = GetAuthenticationTicket(request);
            var user = _users.GetAuthenticatedUser(token);
            return user.UserName;
        }

        private static string GetAuthenticationTicket(IRequest request)
        {
            try
            {
                var tokenCookie = request.Cookies[FormsAuthentication.FormsCookieName];
                if (tokenCookie == null) return null;
                var ticket = FormsAuthentication.Decrypt(tokenCookie.Value);
                return ticket?.UserData;
            }
            catch
            {
                return null;
            }
        }
    }
}