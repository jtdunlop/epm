namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.UI;
    using Infrastructure.Html;

    public class SubscriberSalesItemModel
    {
        public string EveOnlineCharacter { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal GPAmt { get; set; }
    }

    public class ReportTotal
    {
        public decimal Sales { get; set; }
        public decimal? Profit { get; set; }
    }
    
    public class SubscriberSalesModel
    {
        private readonly TableDefinition _tabledef;

        public ReportTotal Total
        {
            get
            {
                return new ReportTotal
                {
                    Sales = Detail.Sum(f => f.GrossAmount),
                    Profit = Detail.Sum(f => f.GpAmt)
                };
            }
        }

        public SubscriberSalesModel()
        {
            _tabledef = new TableDefinition
            {
                Columns = new List<IColumnDefinition>
				{
					new DataColumnDefinition<SubscriberSalesItemModel>(f => f.EveOnlineCharacter),
					new DataColumnDefinition<SubscriberSalesItemModel>(f => f.GrossAmount, null, new FooterDefinition<ReportTotal>(f => f.Sales)),
					new DataColumnDefinition<SubscriberSalesItemModel>(f => f.GPAmt, null, new FooterDefinition<ReportTotal>(f => f.Profit))
				}
            };
        }

        public string TableHtml { get { return TableHtmlGenerator.Generate(_tabledef, Detail, Total); } }
        public List<ItemTransactionBySubscriberDto> Detail { get; set; }
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
    }
}

