﻿namespace DBSoft.EPMWeb.Models.Production
{
    using EPM.DAL.Services.Contracts;
	using EPM.DAL.DTOs;
	using EPM.DAL.Interfaces;
    using System.Linq;
    using EPM.UI;

	public class OutboundContractModel
    {
		private readonly IOrderedEnumerable<OutboundContractDTO> _detail;
		private readonly TableDefinition _tableDef;

		public OutboundContractModel(IContractService service, IConfigurationService config, string token)
		{
			_detail = service.ListOutboundContracts(token)
				.OrderBy(f => f.ItemName);
			_tableDef = new TableDefinition();
			_tableDef.Columns.Add(new DataColumnDefinition<AssetByItemDTO>(f => f.ItemName));

		}

		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}
    }
}
