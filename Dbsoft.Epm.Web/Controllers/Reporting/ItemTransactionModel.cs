﻿namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Infrastructure.Html;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Services.Transactions;
    using DBSoft.EPM.UI;

    public class ItemTransactionModel
	{
		private readonly IEnumerable<ItemTransactionDto> _detail;
		private readonly TableDefinition _tableDef;

		public ItemTransactionModel(IItemTransactionService service, int itemID, DateTime fromDate, DateTime toDate, string token)
		{
			_detail = service
				.List(new ItemTransactionRequest 
				{ 
					Token = token,
					ItemID = itemID, 
					DateRange = new DateRange(fromDate, toDate), 
					TransactionType = TransactionType.Sell
				})
				.OrderByDescending(f => f.DateTime);

			_tableDef = new TableDefinition
			{
				Columns = new List<IColumnDefinition> 
				{ 
					new DataColumnDefinition<ItemTransactionDto>(f => f.ItemName),
					new DataColumnDefinition<ItemTransactionDto>(f => f.DateTime, caption: "Date/Time"),
					new DataColumnDefinition<ItemTransactionDto>(f => f.Quantity),
					new DataColumnDefinition<ItemTransactionDto>(f => f.Price),
					new DataColumnDefinition<ItemTransactionDto>(f => f.Cost),
					new DataColumnDefinition<ItemTransactionDto>(f => f.GpPct),
				}
			};
		}

		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}
	}
}