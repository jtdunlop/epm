﻿namespace Dbsoft.Epm.Test.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Autofac;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Transactions;
    using EntityFramework.Extensions;
    using JetBrains.Annotations;
    using Web.Infrastructure;
    using Xunit;

    public class DatabaseFixture
    {
        public static string Token;

        public DatabaseFixture()
        {
            MapperConfig.ConfigureMappings();
            var container = new IocContainerBuilder()
                .WithModules(new TestDatabaseModule())
                .Build();
            Token = Guid.NewGuid().ToString();
            Authenticate(container);
            InitializeMarketImports(container);
            InitializePurchases(container);
            InitializeBuyOrders(container);
        }

        private static void InitializeBuyOrders(IComponentContext container)
        {
            // Clear Nano-Factories which have many towers that consume them.
            // When no assets a buy order will be triggered even if no sales
            using (var context = container.Resolve<IDbContextFactory>().CreateContext())
            {
                context.Assets.Where(f => f.ItemID == StaticData.Items.BuyOrderMultiMaterial.Id).Delete();
            }
        }

        private static void InitializePurchases(IComponentContext container)
        {
            using (var context = container.Resolve<IDbContextFactory>().CreateContext())
            {
                context.Transactions.Where(f => f.ItemID == StaticData.Items.HighCollateral.Id).Delete();
                context.Transactions.Where(f => f.ItemID == StaticData.Items.LowCollateral.Id).Delete();
            }

            var transactions = container.Resolve<ITransactionService>();
            var request = new List<SaveTransactionRequest>
            {
                new SaveTransactionRequestBuilder()
                    .WithItemId(StaticData.Items.HighCollateral.Id)
                    .WithPrice(StaticData.Items.HighCollateral.Price)
                    .WithTransactionType(TransactionType.Buy)
                    .Build(),
                new SaveTransactionRequestBuilder()
                    .WithItemId(StaticData.Items.LowCollateral.Id)
                    .WithPrice(StaticData.Items.LowCollateral.Price)
                    .WithTransactionType(TransactionType.Buy)
                    .Build()
            };
            transactions.SaveTransactions(Token, request);
        }

        private static long CalculateTransactionId(int item, TransactionType buy)
        {
            return item*10 + (buy == TransactionType.Buy ? 0 : 1);
        }

        private static void InitializeMarketImports(IComponentContext container)
        {
            var importer = container.Resolve<IMarketImportService>();


            var requests = new List<SaveMarketImportRequest>
            {
                new SaveMarketImportRequest
                {
                    ItemID = StaticData.Items.HighCollateral.Id,
                    OrderType = OrderType.Buy,
                    Price = StaticData.Items.HighCollateral.Price,
                    RegionID = StaticData.Regions.Hub,
                    SolarSystemID = StaticData.SolarSystems.Buy,
                    StationID = StaticData.Stations.Buy
                }
            };
            importer.SaveMarketImports(Token, DateTime.UtcNow, requests);
        }

        private static void Authenticate(IComponentContext container)
        {
            var users = container.Resolve<IUserService>();
            users.Authenticate(new SsoAuthenticateRequest
            {
                EveOnlineCharacter = "SJ Astralana",
                SessionToken = Token
            });
        }

        private class SaveTransactionRequestBuilder
        {
            private readonly SaveTransactionRequest _request;

            public SaveTransactionRequestBuilder()
            {
                _request = new SaveTransactionRequest
                {
                    DateTime = DateTime.UtcNow,
                    ItemID = StaticData.Items.HighCollateral.Id,
                    Price = StaticData.Items.HighCollateral.Price,
                    Quantity = 1,
                    TransactionType = TransactionType.Buy,
                    VisibleFlag = true
                };
            }

            public SaveTransactionRequestBuilder WithItemId(int id)
            {
                _request.ItemID = id;
                return this;
            }

            public SaveTransactionRequestBuilder WithPrice(decimal price)
            {
                _request.Price = price;
                return this;
            }

            public SaveTransactionRequestBuilder WithTransactionType(TransactionType type)
            {
                _request.TransactionType = type;
                return this;
            }

            public SaveTransactionRequest Build()
            {
                if (_request.EveTransactionID == 0)
                {
                    _request.EveTransactionID = CalculateTransactionId(_request.ItemID, _request.TransactionType);
                }
                return _request;
            }
        }
    }

    [CollectionDefinition("Database collection"), UsedImplicitly]
    public class DatabaseCollection : ICollectionFixture<DatabaseFixture>
    {
    }
}