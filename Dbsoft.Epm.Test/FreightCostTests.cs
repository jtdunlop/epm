﻿namespace Dbsoft.Epm.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.ItemCosts;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EPM.DAL.Services.MaterialCosts;
    using DBSoft.EPM.DAL.Services.Transactions;
    using NSubstitute;
    using Service;
    using Autofac;
    using DBSoft.EPM.DAL.Services.Materials;
    using Infrastructure;
    using Xunit;
    

    [Collection("Database collection")]
    public class FreightCostTests : IClassFixture<ConfigFixture>
    {
        private readonly IConfigurationService _config;
        private readonly IMarketPriceService _prices;
        private readonly IMaterialCostService _costs;
        private readonly IItemService _items;
        private readonly IBuildMaterialService _materials;
        private readonly IUniverseService _universe;
        private readonly FreightCalculator _calculator;

        private const int Tritanium = 34;
        private const int Zydrine = 39;
        const int Raven = 638;
        const int Missing = 99999;

        public FreightCostTests(ConfigFixture fixture)
        {
            var config = new DBSoft.EPM.DAL.Mvc6.EpmConfig(fixture.GetConfiguration());
            var provider = new ConnectionStringProvider(config);
            var factory = new EpmEntitiesFactory(provider);

            _config = Substitute.For<IConfigurationService>();
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightPickupCost).Returns(3500000M);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightJumpCost).Returns(1500000M);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightMaxVolume).Returns(845000);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightMaxCollateral).Returns(1000000000M);

            var jumps = Substitute.For<IJumpService>();
            jumps.GetJumps(0, 0).ReturnsForAnyArgs(f => (short)3);

            var materials = Substitute.For<IMaterialItemService>();
            var producible = new List<MaterialItemDto> { new MaterialItemDto { Volume = .01M } };
            materials.ListBuildable(null).ReturnsForAnyArgs(f => producible);

            _materials = Substitute.For<IBuildMaterialService>();
            _materials.ListBuildable(Arg.Any<string>()).Returns(new List<BuildMaterialDto> 
            { 
                new BuildMaterialDto { ItemId = Raven, Quantity = 1, MaterialId = Tritanium },
                new BuildMaterialDto { ItemId = Missing, Quantity = 1, MaterialId = Tritanium } 
            });

            _prices = Substitute.For<IMarketPriceService>();
            _prices.List(null).ReturnsForAnyArgs(new List<MarketPriceDTO> 
            { 
                new MarketPriceDTO { ItemID = Tritanium, CurrentPrice = 5 },
                new MarketPriceDTO{ItemID = Zydrine, CurrentPrice = 1500}
            });

            var purchases = Substitute.For<IItemTransactionService>();
            purchases.ListByItem(Arg.Any<ItemTransactionRequest>()).Returns(new List<ItemTransactionByItemDto>());

            var users = Substitute.For<IUserService>();
            users.GetUserID(Arg.Any<string>()).Returns(1);

            _items = Substitute.For<IItemService>();
            _items.ListBuildable(Arg.Any<string>()).Returns(
                new List<BuildableItemDTO> 
                { 
                    new BuildableItemDTO 
                    { 
                        ItemID = Raven, 
                        ItemName = "Raven",
                        QuantityMultiplier = 1, 
                        Volume = 50000 
                    }, 
                    new BuildableItemDTO
                    {
                        ItemID = Missing,
                        ItemName = "Missing",
                        QuantityMultiplier = 1,
                        Volume = 50000
                    }
                });

            _costs = Substitute.For<IMaterialCostService>();
            _costs.List(Arg.Any<MaterialCostRequest>()).Returns(new List<MaterialCostDTO> { new MaterialCostDTO { MaterialID = Tritanium } });

            _universe = Substitute.For<IUniverseService>();
            _universe.GetStation(Arg.Any<int>()).Returns(new StationDTO());
            _universe.ListSolarSystems().Returns(new List<SolarSystemDTO> { new SolarSystemDTO() });
            _universe.GetAdjustedMarketPrices().Returns(new List<MarketAdjustedPriceDTO> 
            { 
                new MarketAdjustedPriceDTO { ItemID = Missing, AdjustedPrice = 200000000 }, 
                new MarketAdjustedPriceDTO { ItemID = Raven, AdjustedPrice = 200000000 },
                new MarketAdjustedPriceDTO { ItemID = Tritanium, AdjustedPrice = 5 }
            });
            _universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());

            _calculator = new FreightCalculator(_config, jumps, _universe);

            using (var context = factory.CreateContext())
            {
                var extension = context.Set<ItemExtension>().Single(f => f.UserID == 1 && f.ItemID == Raven);
                extension.MinimumStock = 8;
                context.SaveChanges();
            }
        }

        [Fact]
        public void WhenFreightIsVolumeConstrained_MaterialCostsShouldReflectFreightVolumeCosts()
        {
            var actual = CreateMaterialServiceResponse(StaticData.Items.LowCollateral.Id);
            var expected = CalculateExpectedCost(StaticData.Items.LowCollateral.Price,
                StaticData.Items.LowCollateral.Volume);
            Assert.Equal(expected.SignificantFigures(4), actual);
        }

        [Fact]
        public void WhenFreightIsCollateralConstrained_MaterialCostsShouldReflectFreightValueCosts()
        {
            var actual = CreateMaterialServiceResponse(StaticData.Items.HighCollateral.Id);
            var expected = CalculateExpectedCost(StaticData.Items.HighCollateral.Price,
                StaticData.Items.HighCollateral.Volume);
            Assert.Equal(expected.SignificantFigures(4), actual);
        }

        private static decimal CreateMaterialServiceResponse(int item)
        {
            var container = new IocContainerBuilder()
                .WithModules(new FreightConfigModule())
                .Build();
            var service = container.Resolve<IMaterialCostService>();
            var request = new MaterialCostRequest {Token = DatabaseFixture.Token};
            var result = service.List(request);
            var actual = result.Single(f => f.MaterialID == item).Cost;
            return actual.SignificantFigures(4);
        }

        private static decimal CalculateExpectedCost(decimal price, decimal volume)
        {
            return price +
                   FreightConfigModule.FreightCost(StaticData.Freight.Jumps) /
                   FreightConfigModule.FreightQuantity(volume, price);
        }

        [Fact]
        public void ItemCostsShouldReflectFreightValueCosts()
        {
            _prices.List(Arg.Any<MarketPriceRequest>()).ReturnsForAnyArgs(new List<MarketPriceDTO> { new MarketPriceDTO { ItemID = Raven, CurrentPrice = 200000000 } });

            var request = new ListBuildableRequest {Token = ""};
            var service = new ItemCostService(_costs, _items, _config, _materials, _universe, _prices, _calculator);
            var result = service.ListBuildable(request);

            Assert.Equal(1600000, result.First(f => f.ItemID == Raven).Cost);
        }

        [Fact]
        public void GivenNotConfigured_CalculatorShouldReturnZero()
        {
            var config = Substitute.For<IConfigurationService>();
            var jumps = Substitute.For<IJumpService>();
            var universe = Substitute.For<IUniverseService>();
            universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());
            var calculator = new FreightCalculator(config, jumps, universe);
            var result = calculator.GetFreightCost("", 1, 1);
            Assert.Equal(0, result);
        }

        [Fact]
        public void WhenMarketPriceNotAvailable_CalculatorShouldUseAdjustedPrice()
        {
            var configService = Substitute.For<IConfigurationService>();

            var service = new ItemCostService(_costs, _items, configService, _materials, _universe, _prices, _calculator);
            var request = new ListBuildableRequest { Token = Guid.NewGuid().ToString() };

            var result = service.ListBuildable(request);
            Assert.True(result.Single(f => f.ItemID == Missing).FreightCost == 1600000);
        }

        private class FreightConfigModule : Module
        {
            private const decimal FreightPickupCost = 3500000;
            private const decimal FreightJumpCost = 1500000;
            private const decimal FreightMaxCollateral = 1000000000;
            private const decimal FreightMaxVolume = 845000;

            public static decimal FreightCost(int jumps)
            {
                return FreightPickupCost + FreightJumpCost * jumps;
            }

            public static decimal FreightQuantity(decimal volume, decimal price)
            {
                return FreightMaxVolume/volume*price >= FreightMaxCollateral
                    ? Math.Floor(FreightMaxCollateral/price)
                    : Math.Floor(FreightMaxVolume/volume);
            }

            protected override void Load(ContainerBuilder builder)
            {
                builder.Register(f =>
                {
                    var config = new ConfigurationServiceBuilder()
                        .WithSetting(ConfigurationType.FreightPickupCost, FreightPickupCost)
                        .WithSetting(ConfigurationType.FreightJumpCost, FreightJumpCost)
                        .WithSetting(ConfigurationType.FreightMaxVolume, FreightMaxVolume)
                        .WithSetting(ConfigurationType.FreightMaxCollateral, FreightMaxCollateral)
                        .WithSetting(ConfigurationType.MarketSellLocation, StaticData.Stations.Sell)
                        .WithSetting(ConfigurationType.FactoryLocation, StaticData.Stations.Build)
                        .Build();
                    return config;
                });
            }
        }
    }


}
