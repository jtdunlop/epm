﻿namespace DBSoft.EPMWeb.Controllers.api
{
    public class SalesModel
    {
        public decimal Sales { get; set; }
        public decimal? Profit { get; set; }
        public decimal? GPPct
        {
            get
            {
                if (Sales == 0)
                {
                    return 0;
                }
                return Profit * 100 / Sales;
            }
        }
    }
}