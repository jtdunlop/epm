﻿namespace DBSoft.EPMWeb.Models.Production
{
    using Annotations;
    using AutoMapper;
	using EPM.DAL.DTOs;
	using EPM.DAL.Interfaces;
	using System.Collections.Generic;
	using System.Linq;

    [UsedImplicitly]
    public class BuildableItemListItemModel
	{
		public int ItemID { get; set; }
		public string ItemName { get; set; }
		public int? MinimumStock { get; set; }
        public decimal? PerJobAdditionalCost { get; set; }
	}

	public class BuildableItemListModel
	{
		public BuildableItemListModel(IItemService service, string token, string returnUrl)
		{
			Mapper.CreateMap<BuildableItemDTO, BuildableItemListItemModel>();

            Detail = Mapper.Map<IEnumerable<BuildableItemDTO>, IEnumerable<BuildableItemListItemModel>>(service.ListMaintainable(token).OrderBy(f => f.ItemName));
			ReturnUrl = returnUrl;
		}
		public IEnumerable<BuildableItemListItemModel> Detail { get; set; }
		public string ReturnUrl{get;set;}
	}
}