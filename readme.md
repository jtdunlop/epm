Installation Guide

You will need Visual Studio 2015 (community edition is fine) and Sql Server Express

Run dnvm use 1.0.0-beta5 -p. This will install a runtime and add dnx and dnu to your path. 

Run script.sql to create a database

Import gates.csv

Update config.json to point to the created database

Install nodejs.

Go into the solution

Make the webjob your default project, go into package manager, set the default project to epm.dal.codefirst and run update-database from there

Press F5 and pray, the app may come up.

Register as a user and proceed.

After you've configured your user, point the webjob app.config to your database. F5 the webjob will populate the market research report.


