﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using DBSoft.EPM.DAL.Extensions;
using DBSoft.EPM.DAL.Interfaces;
using Microsoft.Framework.Configuration;

namespace DBSoft.EPM.DAL.Mvc6
{
    public class EpmConfig : IEpmConfig
    {
        private readonly IConfiguration _config;

        public EpmConfig(IConfiguration config)
        {
            _config = config;
        }

        public T GetSetting<T>(string key) where T : struct
        {
            var setting = GetSetting(key);
            return ConfigurationExtensions.TryParse<T>(setting);
        }

        public string GetSetting(string key)
        {
            return _config.Get(key);
        }

        public string GetConnectionString(string key)
        {
            return _config.Get($"Data:{key}:ConnectionString");
        }

        public string GetBuildVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var version = string.Join(".", FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion.Split('.').ToList().Take(3));
            return version;
        }
    }
}
