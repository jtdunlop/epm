﻿namespace DBSoft.EPMWeb.Models.Reporting
{
	public class ItemCostItemModel
	{
		public string ItemName { get; set; }
		public decimal? Markup { get; set; }
	}
}