/// <vs />
module.exports = function (grunt) {
	grunt.initConfig({
		//this loads our packages for our grunt file
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					{src: 'bower_components/angular/angular.js', dest: 'lib/angular/angular.js'},
					{src: 'bower_components/angular-bootstrap/ui-bootstrap-tpls.js', dest: 'lib/angular-bootstrap/ui-bootstrap-tpls.js' },
					{ src: 'bower_components/angular-resource/angular-resource.js', dest: 'lib/angular-resource/angular-resource.js' },
					{ src: 'bower_components/angular-sanitize/angular-sanitize.js', dest: 'lib/angular-sanitize/angular-sanitize.js' },
					{ src: 'bower_components/angular-ui-date/src/date.js', dest: 'lib/angular-ui-date/date.js' },
					{ src: 'bower_components/bootstrap/dist/js/bootstrap.js', dest: 'lib/bootstrap/bootstrap.js' },
					{ src: 'bower_components/bootstrap/dist/css/bootstrap.css', dest: 'lib/bootstrap/bootstrap.css' },
					{ src: 'bower_components/jquery/dist/jquery.js', dest: 'lib/jquery/jquery.js' },
					{ src: 'bower_components/jquery-ui/themes/base/images/*.png', dest: 'lib/jquery-ui/images/', expand: true, flatten: true},
					{ src: 'bower_components/jquery-ui/themes/base/jquery-ui.css', dest: 'lib/jquery-ui/jquery-ui.css' },
					{ src: 'bower_components/jquery-ui/jquery-ui.js', dest: 'lib/jquery-ui/jquery-ui.js' },
					{ src: 'bower_components/moment/moment.js', dest: 'lib/moment/moment.js' }
				]
			}
		}
	});

	//npm modules need for our task
	grunt.loadNpmTasks('grunt-contrib-copy');
};