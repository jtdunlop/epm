namespace Dbsoft.Epm.Test.Infrastructure
{
    using System.Collections.Generic;
    using Autofac;
    using Microsoft.Framework.Configuration;
    using Microsoft.Framework.DependencyInjection;
    using NSubstitute;
    using Web;

    public class IocContainerBuilder
    {
        private readonly List<Module> _modules;

        public IocContainerBuilder()
        {
            _modules = new List<Module>
            {
                new TestDatabaseModule()
            };
        }

        /// <summary>
        /// Multiple calls are additive
        /// </summary>
        public IocContainerBuilder WithModules(params Module[] modules)
        {
            foreach (var module in modules)
            {
                _modules.Add(module);
            }
            return this;
        }

        public IContainer Build()
        {
            var config = Substitute.For<IConfiguration>();
            var services = Substitute.For<IServiceCollection>();

            return ContainerConfig.BuildContainer(services, config, _modules);
        }
    }
}