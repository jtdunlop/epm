﻿// Heavily modified version of Postsharp.Cache https://www.nuget.org/packages/PostSharp.Cache

namespace DbSoft.Cache.Aspect
{
    using System.Collections.Generic;

    public static class CacheService
    {
		public static string SessionProperty { get; set; }

        private readonly static List<ICache> Caches = new List<ICache>();

        public static ICache GetCache(string name)
        {
            var cache = Caches.Find(f => f.Name == name);
            if (cache != null) return cache;
            var provider = new CacheProvider(name);
            Caches.Add(provider);
            return provider;
        }

        public static void ClearCache(string name)
        {
            var cache = Caches.Find(f => f.Name == name);
            cache?.DeleteAll();
        }

        public static void ClearCache(string name, string @session)
        {
            var cache = Caches.Find(f => f.Name == name);
            cache?.DeleteAll(session);
        }
    }
}
