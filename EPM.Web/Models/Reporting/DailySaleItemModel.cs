namespace DBSoft.EPMWeb.Models.Reporting
{
	using System;
	using System.ComponentModel;
	using System.ComponentModel.DataAnnotations;

	public class DailySaleItemModel
	{
		[DataType(DataType.Date)]
		[DisplayName("Date")]
		public DateTime DateTime { get; set; }
		public decimal GrossAmount { get; set; }
		public decimal GPAmt { get; set; }
		public decimal GPPct { get; set; }
	}
}