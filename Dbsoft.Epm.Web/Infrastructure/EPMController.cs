﻿using DBSoft.EPM.DAL.Services;
using Microsoft.AspNet.Mvc;

namespace Dbsoft.Epm.Web.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using DBSoft.EPM.DAL.Requests;

    public class EpmController : Controller
    {
        private readonly IUserService _users;

        public EpmController(IUserService users)
        {
            _users = users;
        }

        protected bool IsEveClient;
        protected string Token { get; private set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            ViewBag.HideMenu = false;
            IsEveClient = Context.Request.Headers["EVE_TRUSTED"] == "Yes";
            if (Context.Session["Token"] == null)
            {
                Context.Session["Token"] = Guid.NewGuid().ToByteArray();
            }
            Token = new Guid(Context.Session["Token"]).ToString();
            ViewBag.Token = Token;
            if (Context.User.Identity.IsAuthenticated)
            {
                // If I already know you, I already have your refresh token
                Authenticate(Context.User.Claims.ToList());
                ViewBag.IsAdmin = _users.IsAdmin(Token);
            }
            else
            {
                ViewBag.IsAdmin = false;
            }
            base.OnActionExecuting(context);
        }

        protected void Authenticate(List<Claim> claims)
        {
            var user = claims.Single(f => f.Type == ClaimTypes.Name).Value;
            var refresh = claims.SingleOrDefault(f => f.Type == ClaimTypes.UserData);
            var request = new SsoAuthenticateRequest
            {
                EveOnlineCharacter = user,
                SessionToken = Token,
                RefreshToken = refresh?.Value
            };
            _users.Authenticate(request);
        }
    }
}