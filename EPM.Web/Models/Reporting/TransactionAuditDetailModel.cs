namespace DBSoft.EPMWeb.Models.Reporting
{
    public class TransactionAuditDetailModel
    {
        public string UserName { get; set; }
        public string ItemName { get; set; }
        public decimal GrossAmount { get; set; }

    }
}