﻿using DBSoft.EPM.DAL.Services.AccountApi;

namespace DBSoft.EPMWeb.Support.Filters
{
	using System.Web.Mvc;
	using System.Web.Routing;
	using Controllers;
	using EPM.DAL;
	using EPM.DAL.Factories;
	using EPM.DAL.Services;
	using EPM.Logic;

	public class RequireCompleteConfigurationAttribute : ActionFilterAttribute 
	{
		private readonly IConfigurationProcessor _configProcessor;

		public RequireCompleteConfigurationAttribute()
		{
			var factory = new EpmEntitiesFactory(new ConnectionStringProvider(new EpmConfig()));
            var users = new UserService(factory);
			_configProcessor = new ConfigurationProcessor(new ConfigurationService(factory, users), new AccountApiService(factory, users));
		}

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var token = AuthenticationHelper.GetAuthenticationTicket(filterContext.HttpContext);
			if (!UserService.UserIsAuthenticated(token))
			{
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        action = "Logout",
                        controller = "Account"
                    }));
			}
			else if (!_configProcessor.AccountIsAvailable(token))
			{
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
				{
					action = "Accounts",
					controller = "Maintenance"
				}));
			}
			else if (!_configProcessor.ConfigurationIsValid(token))
			{
				filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
					{
						action = "Configuration", 
						controller = "Maintenance"
					}));
			}
			base.OnActionExecuting(filterContext);
		}
	}
}