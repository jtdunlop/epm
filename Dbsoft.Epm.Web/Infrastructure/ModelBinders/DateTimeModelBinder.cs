﻿namespace Dbsoft.Epm.Web.Infrastructure.ModelBinders
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Mvc.ModelBinding;

    public class DateTimeModelBinder : IModelBinder
    {
        public Task<ModelBindingResult> BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof (DateTime) && bindingContext.ModelType != typeof (DateTime?))
                return Task.FromResult<ModelBindingResult>(null);
            var value = (DateTime?)bindingContext.Model;
            if ( value == null )
            {
                return Task.FromResult<ModelBindingResult>(null);
            }
            var result = value.Value.ToUniversalTime();
            return Task.FromResult(new ModelBindingResult(result, bindingContext.ModelName, true));
        }
    }
}