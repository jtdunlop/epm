// Heavily modified version of Postsharp.Cache https://www.nuget.org/packages/PostSharp.Cache

namespace DbSoft.Cache.Aspect.Attributes
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using PostSharp.Aspects;
    using Supporting;

    public static partial class Cache
    {
        [Serializable]
        public class Cacheable : OnMethodBoundaryAspect
        {
            private readonly int _ttl;
            private readonly string _name;
            private KeyBuilder _keyBuilder;

	        private KeyBuilder KeyBuilder => _keyBuilder ?? (_keyBuilder = new KeyBuilder());

            public Cacheable(int ttl = 0, string name = "default")
            {
                _ttl = ttl;
                _name = name;
            }

            //Method executed at build time.
            public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
            {
                KeyBuilder.MethodParameters = method.GetParameters();
	            Debug.Assert(method.DeclaringType != null, "method.DeclaringType != null");
	            KeyBuilder.MethodName = $"{method.DeclaringType.FullName}.{method.Name}";
            }

            // This method is executed before the execution of target methods of this aspect.
            public override void OnEntry(MethodExecutionArgs args)
            {
               // Compute the cache key.
				KeyBuilder.SessionProperty = CacheService.SessionProperty;
                var cacheKey = KeyBuilder.BuildCacheKey(args.Arguments);

                // Fetch the value from the cache.
                var cache = CacheService.GetCache(_name);
                var value = (DateWrapper<object>)(cache.Contains(cacheKey) ? cache[cacheKey] : null);

                if (value != null && !IsTooOld(value.Timestamp))
                {
                    // The value was found in cache. Don't execute the method. Return immediately.
                    args.ReturnValue = value.Object;
                    args.FlowBehavior = FlowBehavior.Return;
                }
                else
                {
                    if ( value != null )
                    {
                        cache.Delete(cacheKey);
                    }
                    // The value was NOT found in cache. Continue with method execution, but store
                    // the cache key so that we don't have to compute it in OnSuccess.
                    args.MethodExecutionTag = cacheKey;
                }
            }

            // This method is executed upon successful completion of target methods of this aspect.
            public override void OnSuccess(MethodExecutionArgs args)
            {
                var cacheKey = (string)args.MethodExecutionTag;
                CacheService.GetCache(_name)[cacheKey] = new DateWrapper<object>
	               {
                    Object = args.ReturnValue,
                    Timestamp = DateTime.UtcNow
                };
            }

            private bool IsTooOld(DateTime time)
            {
                if ( _ttl == 0 )
                {
                    return false;
                }
                return DateTime.UtcNow - time > TimeSpan.FromSeconds(_ttl);                
            }

        }
    }

}

