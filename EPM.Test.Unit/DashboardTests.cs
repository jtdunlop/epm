﻿namespace EPM.Test.Unit
{
    using System.Collections.Generic;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Services.Transactions;
    using DBSoft.EPMWeb.Controllers.api;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class DashboardTests
    {
        [TestMethod]
        public void WhenNoSales_CapitalRatioShouldBeNull()
        {
            var assets = Substitute.For<IAssetCapitalService>();
            var balances = Substitute.For<IAccountBalanceService>();
            var transactions = Substitute.For<IItemTransactionService>();
            transactions.ListByItem(Arg.Any<ItemTransactionRequest>()).Returns(f => new List<ItemTransactionByItemDTO>());
            var builder = new DashboardModelBuilder(balances, transactions, assets);
            var model = builder.CreateModel(null);
            Assert.AreEqual(model.CapitalRatio, null);
        }
    }
}
