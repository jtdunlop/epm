﻿using DBSoft.EPMWeb;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(OwinStartup))]
namespace DBSoft.EPMWeb
{
    using Owin;

    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}