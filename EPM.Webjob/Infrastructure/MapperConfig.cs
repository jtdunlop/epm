﻿namespace EPM.Webjob.Infrastructure
{
    using AutoMapper;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EVEAPI.Entities.MarketOrder;
    using MarketOrder = DBSoft.EVEAPI.Entities.MarketOrder.MarketOrder;

    public static class MapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<Station, StationDTO>()
                .ForMember(m => m.StationID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.StationName, opt => opt.MapFrom(m => m.Name))
                .ForMember(m => m.StationTax, opt => opt.MapFrom(m => m.Tax))
                ;
            Mapper.CreateMap<MarketPrice, MarketAdjustedPriceDTO>();
            Mapper.CreateMap<SolarSystem, SolarSystemDTO>()
                .ForMember(m => m.SolarSystemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.SolarSystemName, opt => opt.MapFrom(m => m.Name))
                ;

            Mapper.CreateMap<User, UserDTO>()
                .ForMember(m => m.UserID, map => map.MapFrom(m => m.ID))
                .ForMember(m => m.UserName, map => map.MapFrom(m => m.EveOnlineCharacter));


            Mapper.CreateMap<MarketOrder, SaveMarketOrderRequest>()
                .ForMember(m => m.OrderID, opt => opt.MapFrom(m => m.ID))
                .ForMember(m => m.OriginalQuantity, opt => opt.MapFrom(m => m.VolumeEntered))
                .ForMember(m => m.RemainingQuantity, opt => opt.MapFrom(m => m.VolumeRemaining))
                .ForMember(m => m.MinimumQuantity, opt => opt.MapFrom(m => m.MinimumVolume))
                .ForMember(m => m.OrderStatus,
                    opt =>
                        opt.ResolveUsing(
                            f => f.OrderState == OrderState.Active ? OrderStatus.Active : OrderStatus.Inactive))
                .ForMember(m => m.EveCharacterID, opt => opt.MapFrom(m => m.CharacterID))
                .ForMember(m => m.Token, opt => opt.Ignore())
                ;


            Mapper.CreateMap<Item, ItemDTO>()
                .ForMember(f => f.ItemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(f => f.ItemName, opt => opt.MapFrom(m => m.Name))
                ;


            Mapper.CreateMap<Item, ItemDTO>()
                .ForMember(f => f.ItemID, opt => opt.MapFrom(m => m.ID))
                .ForMember(f => f.ItemName, opt => opt.MapFrom(m => m.Name))
                ;
        }
    }
}
