﻿namespace Dbsoft.Epm.Web.Infrastructure
{
    using System.Reflection;
    using Autofac;
    using DbSoft.Cache.Aspect;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.Logic;
    using DBSoft.EVEAPI.Crest;
    using Microsoft.Framework.Configuration;
    using Module = Autofac.Module;


    public class AutofacModule : Module
    {
        private readonly IConfiguration _config;

        public AutofacModule(IConfiguration config)
        {
            _config = config;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => _config).As<IConfiguration>();
            // Dbsoft.Epm.Web
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces();
            // DBSoft.EPM.DAL
            builder.RegisterAssemblyTypes(typeof (EpmEntitiesFactory).Assembly)
                .AsImplementedInterfaces();
            // DBSoft.EVE.API
            builder.RegisterAssemblyTypes(typeof (UserAuth).Assembly)
                .Where(f => !f.Name.StartsWith("Mock"))
                .AsImplementedInterfaces();
            // DBSoft.EPM.Logic
            builder.RegisterAssemblyTypes(typeof (ImportManager).Assembly)
                .AsImplementedInterfaces();

            builder.RegisterType<ImportManager>().As<IImportManager>().SingleInstance();
        }
    }
}
