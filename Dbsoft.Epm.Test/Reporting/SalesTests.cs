﻿namespace Dbsoft.Epm.Test.Reporting
{
    using System;
    using System.Linq;
    using Autofac;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.Services.Transactions;
    using Infrastructure;
    using Xunit;

    [Collection("Database collection")]
    public class SalesReportingTests
    {
        [Fact]
        public void TransactionsForDateShouldReturnThatDateOnly()
        {
            var container = new IocContainerBuilder().Build();
            var service = container.Resolve<IItemTransactionService>();
            var range = new DateRange(DateTime.UtcNow, DateTime.UtcNow);
            var result = service.List(new ItemTransactionRequest
            {
                Token = DatabaseFixture.Token,
                DateRange = range,
                TransactionType = TransactionType.Sell,
                ItemID = StaticData.Items.Sold.Id
            }).OrderBy(o => o.DateTime);
            Assert.True(result.Any());
            Assert.True(result.All(f => f.DateTime >= range.StartDate && f.DateTime <= range.EndDate));
        }
    }
}
